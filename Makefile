default:
	mkdir -p target/
	convert logo.svg target/logo.png
	convert -transparent white target/logo.png target/logo.png
	pdflatex -interaction=nonstopmode -output-directory=target/ spec.tex
	pdflatex -interaction=nonstopmode -output-directory=target/ spec.tex

newspec:	
	mkdir -p target/
	pdflatex -interaction=nonstopmode -output-directory=target/ nspec.tex
	pdflatex -interaction=nonstopmode -output-directory=target/ nspec.tex

clean:
	rm -rf target/
