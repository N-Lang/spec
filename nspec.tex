\documentclass{article}

\usepackage[margin=1in]{geometry}
\usepackage[hidelinks]{hyperref}
\usepackage{nattgram}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{longtable}

\title{The N Programming Language Specification}
\author{The N Language Team}

\begin{document}
    \maketitle

    \includegraphics[width=\textwidth]{target/logo.png}

    \vspace*{\fill}

    \textbf{The N Language Team:} Andrew Shin, Justin Thomas, Kimberlee Model,
    Michael Minnick, Shantanu Gupta, Siddhanathan Shanmugam, Victor Cushman,
    Vivian Zhou.

    \newpage
    \subsection{Table of Contents}
    \tableofcontents
    \newpage
    \subsection{Revision History}
    \begin{longtable}{|p{0.10\textwidth}|p{0.10\textwidth}|p{0.45\textwidth}|p{0.25\textwidth}|}
        \hline
        \textbf{Revision} & \textbf{Date} & \textbf{Description} & \textbf{Author} \\
        \hline
        0.0.0 & 2019-02-24 & Initial draft of Attribute Grammar & Kimberlee Model, Siddhanathan Shanmugam \\
        \hline
        0.0.1 & 2019-04-15 & Add phased control flow for attribute evaluation & Kimberlee Model \\
        \hline
        0.0.2 & 2019-05-20 & Boolean, Equality, and Comparison expressions & Kimberlee Model \\
        \hline
        0.1.0 & 2019-06-02 & Restructuring and clarifications on the specification & Kimberlee Model \\
        \hline
    \end{longtable}
    \newpage

    \section{Introduction}
    N is a highly opinionated systems programming language that optimizes for ease of learning while maintaining the desirable characteristics of the C programming language.
    N is intended to be a common sense alternative to C, which retains C’s multiple decades of rich infrastructure while providing a powerful set of tools to develop and maintain modern software.
    The language provides strong static typing, zero-cost interoperability with C, and emphasizes readability and clear semantic meaning over gimmicks and overuse of abstraction.
    In  particular, the N-Lang motto is that there is no magic.
    A developer who understands how their hardware works should be able to write robust and efficient programs that convey clear semantic meaning within their source code.\\

    \section{About This Specification}
    This specification is intended to unambiguously define the N Programming Language.
    In order to do so, it will first look at the representation of primitive data types, followed by definitions of a few special tokens, and lastly the syntax and semantics of the language.
    Most of the spec will be written using attribute grammars.
    However, along the way there will be some prose intended to clarify the given grammar, as well as extensions to the attribute grammar itself.\\

    The syntax is described by each context free grammar production.
    The left hand side names a valid node of the abstract syntax tree.
    The right hand side lists the allowed children.
    A terminal is a leaf node of the tree, and consists of a sequence of characters, and are notated as teletype surrounded by single quotes.
    Special terminals, such as identifiers or literals, indicate that they can take multiple forms and are notated as names surrounded by square brackets.
    Non terminals are sub productions which are embedded within larger ones. They are notated by their names surrounded by angle brackets.
    Here is an example.\\

    \begin{syntax}
        \prdn{\nt{production}{1}}{\tm{terminal} \st{special-terminal} \nt{non-terminal}{1}}
    \end{syntax}
    
    Each special terminal is defined separately using a regular expression to match correct syntax.\\

    \begin{syntax}
        \prdn{\st{special-terminal}}{\regex{Regular Expression}}
    \end{syntax}

    The expression semantics are supplied by the (indented) attributes.
    An attribute attaches a value to a node, each node having multiple attributes to define its type, value and other things.
    Each non terminal is numbered, so that duplicates of the same non terminal may be identified.
    An attribute assigned from the higher node (left hand side) to the lower node (right hand side) is an inherited attribute.
    From a lower node to a higher node is a synthesized node.\\

    \begin{syntax}
        \prdn{\nt{left}{1}}{\nt{right}{1} \nt{left}{2}}
    \end{syntax}
    \begin{semantic}
        \attr{\nt{right}{1}\att{type}}{\nt{left}{1}\att{type}}
        \attr{\nt{left}{2}\att{type}}{\nt{left}{1}\att{type}}
        \attr{\nt{left}{1}\att{value}}{\nt{right}{1}\att{value}}
        \attr{\nt{left}{2}\att{value}}{\nt{right}{1}\att{value}}
    \end{semantic}

    
    An assertion checks the validity of attributes.
    This is how semantic errors are checked.
    In this example, if the \textit{type} attribute is not \texttt{bool}, then a semantic error would occur.\\

    \begin{semantic}
        \asrt{\nt{left}{1}\att{type} = \tm{bool}}
    \end{semantic}

    The symbol table maps identifiers, or variable names, to attributes such as value or type.
    It is also staged in a linked list, so that when scope blocks are entered and exited, the variables contained may be grouped easily.
    The relevant symbol table is accessed as the \textit{SymTbl} attribute of a node, and is inherited from one's parent node.\\

    The symbol table has the typical \textit{has}, \textit{get}, and \textit{put} operations of a \textit{map} or \textit{dictionary} abstract data type.
    It also has the \textit{extend} operation which creates a new table, with a reference to the original table.
    This is used when a new scope block is created inside of another scope block.\\
\end{document}
