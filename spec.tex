\documentclass{article}

\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{attgram}
\usepackage[hidelinks]{hyperref}

%\renewcommand{\familydefault}{\sfdefault}

\title{The N Programming Language Specification}
\author{The N Language Team}

\begin{document}
    \maketitle

    \includegraphics[width=\textwidth]{target/logo.png}

    \vspace*{\fill}

    \textbf{The N Language Team:} Andrew Shin, Justin Thomas, Kimberlee Model,
    Michael Minnick, Shantanu Gupta, Siddhanathan Shanmugam, Victor Cushman,
    Vivian Zhou.

    \newpage
    \subsection{Table of Contents}
    \tableofcontents
    \newpage
    \subsection{Revision History}
    \begin{longtable}{|p{0.10\textwidth}|p{0.10\textwidth}|p{0.45\textwidth}|p{0.25\textwidth}|}
        \hline
        \textbf{Revision} & \textbf{Date} & \textbf{Description} & \textbf{Author} \\
        \hline
        0.0.0 & 2019-02-24 & Initial draft of Attribute Grammar & Kimberlee Model, Siddhanathan Shanmugam \\
        \hline
        0.0.1 & 2019-04-15 & Add phased control flow for attribute evaluation & Kimberlee Model \\
        \hline
        0.0.2 & 2019-05-20 & Boolean, Equality, and Comparison expressions & Kimberlee Model \\
        \hline
        0.0.3 & 2019-06-02 & Rewrite the introduction and about spec sections & Kimberlee Model \\
        \hline
    \end{longtable}

    \vspace*{\fill}

    \copyright 2019 The N Language Team.\\

    This document is free to be shared and edited under the terms of the Creative Commons Attribution, Non-Commercial, Share Alike 3.0 license.
    For an explation of your rights under this license please visit the Creative Commons\\

    https://creativecommons.org/licenses/by-nc-sa/3.0/\\

    \newpage

    \section{Introduction}
    N is a highly opinionated systems programming language that optimizes for ease of learning while maintaining the desirable characteristics of the C programming language.
    N is intended to be a common sense alternative to C, which retains C’s multiple decades of rich infrastructure while providing a powerful set of tools to develop and maintain modern software.
    The language provides strong static typing, zero-cost interoperability with C, and emphasizes readability and clear semantic meaning over gimmicks and overuse of abstraction.
    In  particular, the N-Lang motto is that there is no magic.
    A developer who understands how their hardware works should be able to write robust and efficient programs that convey clear semantic meaning within their source code.\\

    \section{About This Specification}
    This specification is intended to unambiguously define the N Programming Language.
    In order to do so, it will first look at the representation of primitive data types, followed by definitions of a few special tokens, and lastly the syntax and semantics of the language.
    Most of the spec will be written using attribute grammars.
    However, along the way there will be some prose intended to clarify the given grammar, as well as extensions to the attribute grammar itself.\\

    The syntax is described by each context free grammar production.
    The left hand side names a valid node of the abstract syntax tree.
    The right hand side lists the allowed children.
    A terminal is a leaf node of the tree, and consists of a sequence of characters, and are notated as teletype surrounded by single quotes.
    Special terminals, such as identifiers or literals, indicate that they can take multiple forms and are notated as names surrounded by square brackets.
    Non terminals are sub productions which are embedded within larger ones. They are notated by their names surrounded by angle brackets.
    Here is an example.\\

    \begin{syntax}
        \prdn{\nt{production}{1}}{\tm{terminal} \st{special-terminal} \nt{non-terminal}{1}}
    \end{syntax}
    
    Each special terminal is defined separately using a regular expression to match correct syntax.\\

    \begin{syntax}
        \prdn{\st{special-terminal}}{\regex{Regular Expression}}
    \end{syntax}

    The expression semantics are supplied by the (indented) attributes.
    An attribute attaches a value to a node, each node having multiple attributes to define its type, value and other things.
    Each non terminal is numbered, so that duplicates of the same non terminal may be identified.
    An attribute assigned from the higher node (left hand side) to the lower node (right hand side) is an inherited attribute.
    From a lower node to a higher node is a synthesized node.\\

    \begin{syntax}
        \prdn{\nt{left}{1}}{\nt{right}{1} \nt{left}{2}}
    \end{syntax}
    \begin{semantic}
        \attr{\nt{right}{1}\att{type}}{\nt{left}{1}\att{type}}
        \attr{\nt{left}{2}\att{type}}{\nt{left}{1}\att{type}}
        \attr{\nt{left}{1}\att{value}}{\nt{right}{1}\att{value}}
        \attr{\nt{left}{2}\att{value}}{\nt{right}{1}\att{value}}
    \end{semantic}

    
    An assertion checks the validity of attributes.
    This is how semantic errors are checked.
    In this example, if the \textit{type} attribute is not \texttt{bool}, then a semantic error would occur.\\

    \begin{semantic}
        \asrt{\nt{left}{1}\att{type} = \tm{bool}}
    \end{semantic}

    The symbol table maps identifiers, or variable names, to attributes such as value or type.
    It is also staged in a linked list, so that when scope blocks are entered and exited, the variables contained may be grouped easily.
    The relevant symbol table is accessed as the \textit{SymTbl} attribute of a node, and is inherited from one's parent node.\\

    The symbol table has the typical \textit{has}, \textit{get}, and \textit{put} operations of a \textit{map} or \textit{dictionary} abstract data type.
    It also has the \textit{extend} operation which creates a new table, with a reference to the original table.
    This is used when a new scope block is created inside of another scope block.\\

    \section{Tokens}
    \subsection{Keywords}
    The following keywords are defined by the N Language.
    Their meaning will be specified in the attribute grammar, this table may be incomplete or overcomplete and is provided for reference only.\\

    \begin{longtable}{p{0.2\textwidth}p{0.2\textwidth}p{0.2\textwidth}p{0.2\textwidth}}
        \verb|alias| & \verb|as| & \verb|ascii| & \verb|bool| \\
        \verb|break| & \verb|continue| & \verb|countof| & \verb|defer| \\
        \verb|elif| & \verb|else| & \verb|enum| & \verb|export| \\
        \verb|false| & \verb|func| & \verb|if| & \verb|iface| \\
        \verb|import| & \verb|in| & \verb|introduce| & \verb|isolate| \\
        \verb|let| & \verb|loop| & \verb|mut| & \verb|null|\\
        \verb|return| & \verb|s| & \verb|s128| & \verb|s16| \\
        \verb|s32| & \verb|s64| & \verb|s8| & \verb|sizeof| \\
        \verb|struct| & \verb|true| & \verb|typeof| & \verb|u| \\
        \verb|u128| & \verb|u16| & \verb|u32| & \verb|u64| \\
        \verb|u8| & \verb|unique| & \verb|union| & \verb|variant| \\
        \verb|void| & & & \\
    \end{longtable}

    The following keywords are not used by N currently, but are reserved for future use.
    Additional keywords may be reserved in the future.\\

    \begin{longtable}{p{0.2\textwidth}p{0.2\textwidth}p{0.2\textwidth}p{0.2\textwidth}}
        \verb|atomic| & \verb|f128| & \verb|f16| & \verb|f32| \\
        \verb|f64| & \verb|volatile| & & \\
    \end{longtable}

    \subsection{Operators}
    TODO

    \subsection{Special Tokens}
    \subsubsection{Identifiers}
    The following tokens are defined using regular expressions, and are considered special, as they can carry additional meaning.\\

    Identifiers are the names of types, functions and variables.
    They are the keys into the program's symbol tables.\\
    \begin{syntax}
        \prdn{\st{identifier}}{\regex{[a-zA-Z\_][a-zA-Z1-9\_]*}}
    \end{syntax}

    Identifiers matching the following pattern are reserved for use by implementation specifics of N compilers and interpreters.\\

    \begin{syntax}
        \prdn{\st{identifier}}{\regex{\_\_N\_[a-zA-Z1-9\_]*}}
    \end{syntax}

    Identifiers matching the following pattern are reserved for use by implementation specifics of the N Standard Library.
    Aside from the reservation identifiers, the N Standard Library is outside the scope of this specification.\\

    \begin{syntax}
        \prdn{\st{identifier}}{\regex{\_\_libn\_[a-zA-Z1-9\_]*}}
    \end{syntax}

    \subsubsection{Integer Literals}
    The following are specifications for literal integers in bases 16, 8, 2, and 10.
    Integer literals in N may have a lexical sign to indicate positivity or negativity, and underscores within the body of integer literals have no meaning, but are allowed for visually grouping numerals.
    Their semantics, aside from base-prefixes, match the convention of reading numbers from left to right, using arabic numerals with alphabetic characters filling in for higher numerals.
    Each integer literal is given a numeric \textit{value} attribute, holding its numerical value.\\
    Optionally, a numeral may have a type suffix, indicating the literal's type.

    \begin{syntax}
        \prdn{\st{literal-int}}{\regex{[+-]?0(x|X)[0-9a-fA-F]([0-9a-fA-F\_]*[0-9a-fA-F])?((u|s)(8|16|32|64|128)?)?}}
    \end{syntax}

    \begin{syntax}
        \prdn{\st{literal-int}}{\regex{[+-]?0o[0-7]([0-7\_]*[0-7])?((u|s)(8|16|32|64|128)?)?}}
    \end{syntax}

    \begin{syntax}
        \prdn{\st{literal-int}}{\regex{[+-]?0(b|B)[0-1]([0-1\_]*[0-1])?((u|s)(8|16|32|64|128)?)?}}
    \end{syntax}

    \begin{syntax}
        \prdn{\st{literal-int}}{\regex{[+-]?[0-9]([0-9\_]*[0-9])?((u|s)(8|16|32|64|128)?)?}}
    \end{syntax}

    \subsubsection{ASCII Character Literals}
    TODO

    \subsection{Comments}
    TODO

    \section{Primitive Data Types}
    N defines primitive data types for integers, ascii characters, boolean logic.
    Currently N does not define floating point numbers, but it does reserve keywords to define them in the future.

    \subsection{Mutability}
    TODO: variables are default const: cannot be modified after initialization. allowed to mutate after initialization if type qualified with \texttt{mut}.

    \subsection{Integer Types}
    TODO: unsigned integers as \texttt{u}, signed integers as \texttt{s}, sized integers by suffixing with a bitwidth of \texttt{8}, \texttt{16}, \texttt{32}, \texttt{64}, or \texttt{128}.\\

    \begin{syntax}
        \prdn{\nt{int-type}{1}}{\tm{s8}}
    \end{syntax}

    \begin{syntax}
        \prdn{\nt{int-type}{1}}{\tm{s16}}
    \end{syntax}

    \begin{syntax}
        \prdn{\nt{int-type}{1}}{\tm{s32}}
    \end{syntax}

    \begin{syntax}
        \prdn{\nt{int-type}{1}}{\tm{s64}}
    \end{syntax}

    \begin{syntax}
        \prdn{\nt{int-type}{1}}{\tm{s128}}
    \end{syntax}

    \begin{syntax}
        \prdn{\nt{int-type}{1}}{\tm{u}}
    \end{syntax}

    \begin{syntax}
        \prdn{\nt{int-type}{1}}{\tm{u8}}
    \end{syntax}

    \begin{syntax}
        \prdn{\nt{int-type}{1}}{\tm{u16}}
    \end{syntax}

    \begin{syntax}
        \prdn{\nt{int-type}{1}}{\tm{u32}}
    \end{syntax}

    \begin{syntax}
        \prdn{\nt{int-type}{1}}{\tm{u64}}
    \end{syntax}

    \begin{syntax}
        \prdn{\nt{int-type}{1}}{\tm{u128}}
    \end{syntax}

    \begin{syntax}
        \prdn{\nt{int-type}{1}}{\tm{u}}
    \end{syntax}

    \begin{syntax}
        \prdn{\nt{type}{1}}{\nt{int-type}{1}}
    \end{syntax}

    \subsection{ASCII Character Type}
    TODO: The \texttt{ascii} type holds a single character. Sugar for arrays of ascii characters delimited by \texttt{"}.

    \subsection{Boolean Type}
    TODO: a \texttt{bool} typed variable can be assigned only the values \texttt{true} and \texttt{false}

    The \texttt{bool} type takes on the values of \texttt{true} and \texttt{false} only, and form the basis for conditions and control flow in N-Lang.

    \begin{syntax}
        \prdn{\nt{type}{1}}{\tm{bool}}
    \end{syntax}

    \begin{syntax}
        \prdn{\nt{literal-bool}{1}}{\tm{true}}
    \end{syntax}

    \begin{syntax}
        \prdn{\nt{literal-bool}{1}}{\tm{false}}
    \end{syntax}

    \subsection{Composed Types}
    TODO

    \subsection{Arrays}
    TODO

    \subsection{Pointers}
    TODO

    \subsection{The Void Type}
    TODO: the \texttt{void} type is the explicit lack of a type.

    \section{Control Flow}
    TODO: this section is under construction. don't look at it yet.

    \begin{syntax}
        \prdn{\nt{stmts}{1}}{\nt{stmt}{1} \nt{stmts}{2}}
    \end{syntax}

    The type checking phase:
    \begin{semantic}
        \attr{\nt{stmt}{1}\att{itype}}{\nt{stmts}{1}\att{itype}}
        \attr{\nt{stmts}{2}\att{itype}}{\nt{stmts}{1}\att{itype}}
        \tc{stmt}{1}
        \tc{stmts}{2}
        \asrt{\nt{stmt}{1}\att{stype} = $ undefined $ \vee \nt{stmts}{2}\att{stype} = $ undefined $ \vee \nt{stmt}{1}\att{stype} = \nt{stmts}{2}\att{stype}}
        \If{\nt{stmt}{1}\att{stype} \neq $ undefined $}
        \attr{\nt{stmts}{1}\att{stype}}{\nt{stmt}{1}\att{stype}}
        \Elif{\nt{stmts}{2}\att{stype} \neq $ undefined $}
        \attr{\nt{stmts}{1}\att{stype}}{\nt{stmts}{2}\att{stype}}
        \Else
        \attr{\nt{stmts}{1}\att{stype}}{$ undefined$}
        \Endif
    \end{semantic}

    The runtime phase
    \begin{semantic}
        \If{\neg\nt{stmt}{1}\att{postexec}}
        \run{stmt}{1}
        \Endif
        \If{\neg\nt{stmt}{1}\att{break}}
        \run{stmts}{2}
        \attr{\nt{stmts}{1}\att{value}}{\nt{stmts}{2}\att{value}}
        \Else
        \attr{\nt{stmts}{1}\att{value}}{\nt{stmt}{1}\att{value}}
        \Endif
        \If{\nt{stmt}{1}\att{postexec}}
        \run{stmt}{1}
        \Endif
    \end{semantic}

    \begin{syntax}
        \prdn{\nt{stmts}{1}}{\nt{stmt}{1}}
    \end{syntax}

    The type checking phase:
    \begin{semantic}
        \attr{\nt{stmt}{1}\att{itype}}{\nt{stmts}{1}}
        \tc{stmt}{1}
        \attr{\nt{stmts}{1}\att{stype}}{\nt{stmt}{1}\att{itype}}
    \end{semantic}

    The runtime phase:
    \begin{semantic}
        \run{stmt}{1}
        \attr{\nt{stmts}{1}\att{value}}{\nt{stmt}{1}\att{value}}
    \end{semantic}

    \begin{syntax}
        \prdn{\nt{stmt}{1}}{\tm{let} \id{1} \tm{:} \nt{type}{1} \tm{=} \nt{expr}{1} \tm{;}}
    \end{syntax}

    \begin{semantic}
        \asrt{\neg \tblhas{1}}
        \attr{\nt{expr}{1}\att{itype}}{\nt{type}{1}}
        \tblput{1}{\nt{type}{1}}{\nt{expr}{1}\att{value}}
        \attr{\nt{stmt}{1}\att{stype}}{\nt{expr}{1}\att{stype}}
        \attr{\nt{stmt}{1}\att{value}}{\nt{expr}{1}\att{value}}
    \end{semantic}

    The default type checking phase:
    \begin{semantic}
        \asrt{\neg \tblhas{1}}
    \end{semantic}

    \begin{syntax}
        \prdn{\nt{stmt}{1}}{\nt{expr}{1}}
    \end{syntax}

    \begin{semantic}
        \attr{\nt{expr}{1}\att{itype}}{\st{undefined}}
        \attr{\nt{stmt}{1}\att{stype}}{\nt{expr}{1}\att{stype}}
        \attr{\nt{stmt}{1}\att{value}}{\nt{expr}{1}\att{value}}
    \end{semantic}

    \section{Expressions}
    An expression is a piece of code which performs a calculation or causes an action.
    Because certain expressions (such as integer literals) can have type inferrence, there is a complicated scheme of type checking expressions.
    Most expressions will have an ``inherited type'' (represented as an \textit{itype} attribute) and a ``synthesized type'' (represented as an \textit{stype} attribute), based on what type it is expected to be, and what type it actually is.
    The primary source for an \textit{stype} is a variable's type when looked up in a symbol table, or the \textit{type} symbol of an \textit{as-expr}.
    The primary source of an \textit{itype} is the expression which contains the current expression.
    If the \textit{stype} and the \textit{itype} are found not to match, then that constitutes a semantic error in the program.\\

    Not all expressions will always have an \textit{itype}
    In the cases where an \textit{itype} is undefined, the \textit{stype} is allowed to mismatch the \textit{itype}, and is assumed to match the notion of ``what the \textit{itype} should have been''.\\

    Similarly, an expression's \textit{stype} is not always obvious.
    For this reason, type checking is allowed to occur in up to two phases: the \textit{regular} phase, and the \textit{fallback} phase: for when the \textit{stype} is indeterminable during the \textit{regular} phase.
    If the \textit{stype} is indeterminable after the \textit{fallback} phase, then it is considered a semantic error, however considerable effort has been made to eliminate this possibility.\\

    % The pass-through expression macros.
    \newcommand{\passtc}[3]
    {
        The regular type checking phase:
        \begin{semantic}
            \attr{\nt{#2}{1}\att{itype}}{\nt{#1}{1}\att{itype}}
            \tcr{#2}{1}
            \attr{\nt{#1}{1}\att{stype}}{\nt{#2}{1}\att{stype}}
            \asrt{\nt{#1}{1}\att{itype} = $ undefined $ \vee \nt{#1}{1}\att{stype} = \nt{#1}{1}\att{itype}}
            #3
        \end{semantic}
    }

    \newcommand{\passfalltc}[3]
    {
        The fallback type checking phase:
        \begin{semantic}
            \attr{\nt{#2}{1}\att{itype}}{\nt{#1}{1}\att{itype}}
            \tcf{#2}{1}
            \attr{\nt{#1}{1}\att{stype}}{\nt{#2}{1}\att{stype}}
            \asrt{\nt{#1}{1}\att{stype} \neq $ undefined $}
            \asrt{\nt{#1}{1}\att{itype} = $ undefined $ \vee \nt{#1}{1}\att{stype} = \nt{#1}{1}\att{itype}}
            #3
        \end{semantic}
    }

    \newcommand{\passrun}[3]
    {
        The runtime phase:
        \begin{semantic}
            \run{#2}{1}
            \attr{\nt{#1}{1}\att{value}}{\nt{#2}{1}\att{value}}
            #3
        \end{semantic}
    }

    \newcommand{\exprpass}[2]
    {
        \begin{syntax}
            \prdn{\nt{#1-expr}{1}}{\nt{#2-expr}{1}}
        \end{syntax}

        \passtc{#1-expr}{#2-expr}{}

        \passfalltc{#1-expr}{#2-expr}{}

        \passrun{#1-expr}{#2-expr}{}
    }

    \subsection{Boolean Expressions}
    Boolean expressions are expressions composed using boolean values of only \texttt{true} and \texttt{false}.

    \newcommand{\boolexpr}[4]
    {
        \begin{syntax}
            \prdn{\nt{#1-expr}{1}}{\nt{#1-expr}{2} \tm{#2} \nt{#3-expr}{1}}
        \end{syntax}

        The default type checking phase:
        \begin{semantic}
            \asrt{\nt{#1-expr}{1}\att{itype} = \tm{bool} \vee \nt{#1-expr}{1}\att{itype} = $ undefined$}
            \attr{\nt{#1-expr}{2}\att{itype}}{\tm{bool}}
            \attr{\nt{#3-expr}{1}\att{itype}}{\tm{bool}}
            \tcr{#1-expr}{2}
            \tcr{#3-expr}{1}
            \asrt{\nt{#1-expr}{2}\att{stype} = \tm{bool}}
            \asrt{\nt{#3-expr}{1}\att{stype} = \tm{bool}}
            \attr{\nt{#1-expr}{1}\att{stype}}{\tm{bool}}
        \end{semantic}

        The fallback type checking phase:
        \begin{semantic}
            \tcf{#1-expr}{2}
            \tcf{#3-expr}{1}
            \asrt{\nt{#1-expr}{1}\att{stype} = \tm{bool}}
            \asrt{\nt{#1-expr}{2}\att{stype} = \tm{bool}}
            \asrt{\nt{#3-expr}{1}\att{stype} = \tm{bool}}
        \end{semantic}

        The runtime phase:
        \begin{semantic}
            \run{#1-expr}{2}
            \If{\nt{#1-expr}{2}\att{value} = \tm{#4}}
            \attr{\nt{#1-expr}{1}\att{value}}{\tm{#4}}
            \Else
            \run{#3-expr}{1}
            \attr{\nt{#1-expr}{1}\att{value}}{\nt{#3-expr}{1}\att{value}}
            \Endif
        \end{semantic}
    }

    Boolean Expressions in N-Lang, by default take dysjunctive normal form.
    That is that ``or'' expressions have higher precedence than ``and'' expressions.
    And and Or expressions must always short circuit in N-Lang.

    \subsubsection{Or Expressions}
    \boolexpr{or}{||}{and}{true}
    \exprpass{or}{and}

    \subsubsection{And Expressions}
    \boolexpr{and}{\&\&}{not}{false}
    \exprpass{and}{not}

    \subsubsection{Not Expressions}
    Not expressions simply negate boolean expressions, or they are pass through.

    \begin{syntax}
        \prdn{\nt{not-expr}{1}}{\tm{!} \nt{bool-expr}{1}}
    \end{syntax}

    The default type checking phase:
    \begin{semantic}
        \asrt{\nt{not-expr}{1}\att{itype} = \tm{bool} \vee \nt{not-expr}{1}\att{itype} = $ undefined$}
        \attr{\nt{bool-expr}{1}\att{itype}}{\tm{bool}}
        \tcr{bool-expr}{1}
        \asrt{\nt{bool-expr}{1}\att{stype} = \tm{bool}}
        \attr{\nt{not-expr}{1}\att{stype}}{\tm{bool}}
    \end{semantic}

    The fallback type checking phase:
    \begin{semantic}
        \tcf{bool-expr}{1}
        \asrt{\nt{not-expr}{1}\att{stype} = \tm{bool}}
        \asrt{\nt{bool-expr}{1}\att{stype} = \tm{bool}}
    \end{semantic}

    The runtime phase:
    \begin{semantic}
        \run{bool-expr}{1}
        \If{\nt{bool-expr}{1}\att{value} = \tm{true}}
        \attr{\nt{not-expr}{1}\att{value}}{\tm{false}}
        \Else
        \attr{\nt{not-expr}{1}\att{value}}{\tm{true}}
        \Endif
    \end{semantic}

    \exprpass{not}{bool}

    Boolean leaf expressions in N-Lang take the form of either boolean literals, or equality expressions.
    By pass through, this also allows boolean leaf expressions to be parenthesized expressions and as expressions.

    \begin{syntax}
        \prdn{\nt{bool-expr}{1}}{\nt{literal-bool}{1}}
    \end{syntax}

    The default type checking phase:
    \begin{semantic}
        \asrt{\nt{bool-expr}{1}\att{itype} = \tm{bool} \vee \nt{bool-expr}{1}\att{itype} = $ undefined$}
        \attr{\nt{bool-expr}{1}\att{stype}}{\tm{bool}}
    \end{semantic}

    The fallback type checking phase:
    \begin{semantic}
        \asrt{\nt{bool-expr}{1}\att{stype} = \tm{bool}}
    \end{semantic}

    The runtime phase:
    \begin{semantic}
        \attr{\nt{bool-expr}{1}\att{value}}{\nt{literal-bool}{1}\att{value}}
    \end{semantic}

    \exprpass{bool}{equality}

    \subsection{Equality Expressions}
    Because the boolean family expressions are not using the fallback type checking phase, the equality expressions must fallback for themselves to ensure higher precedence operators are fully typechecked.

    \newcommand{\eqexpr}[4]
    {
        \begin{syntax}
            \prdn{\nt{#1-expr}{1}}{\nt{#1-expr}{2} \tm{#2} \nt{#3-expr}{1}}
        \end{syntax}

        The default type checking phase:
        \begin{semantic}
            \asrt{\nt{#1-expr}{1}\att{itype} = \tm{bool} \vee \nt{#1-expr}{1}\att{itype} = $ undefined$}
            \attr{\nt{#1-expr}{2}\att{itype}}{$ undefined$}
            \attr{\nt{#3-expr}{1}\att{itype}}{$ undefined$}
            \tcr{#1-expr}{2}
            \tcr{#3-expr}{1}
            \If{\nt{#1-expr}{2}\att{stype} = $ undefined $ \vee \nt{#3-expr}{1}\att{stype} = $ undefined$}
            \tcf{#1-expr}{1}
            \Else
            \attr{\nt{#1-expr}{1}\att{stype}}{\tm{bool}}
            \Endif
        \end{semantic}

        The fallback type checking phase:
        \begin{semantic}
            \asrt{\nt{#1-expr}{1}\att{itype} = \tm{bool} \vee \nt{#1-expr}{1}\att{itype} = $ undefined$}
            \If{\nt{#1-expr}{2}\att{stype} = $ undefined $ \wedge \nt{#3-expr}{1}\att{stype} \neq $ undefined$}
            \attr{\nt{#1-expr}{2}\att{itype}}{\nt{#3-expr}{1}\att{stype}}
            \tcf{#1-expr}{2}
            \Elif{\nt{#1-expr}{2}\att{stype} \neq $ undefined $ \wedge \nt{#3-expr}{1}\att{stype} = $ undefined$}
            \attr{\nt{#3-expr}{1}\att{itype}}{\nt{#1-expr}{2}\att{stype}}
            \tcf{#3-expr}{1}
            \Endif
            \asrt{\nt{#1-expr}{2}\att{stype} \neq $ undefined$}
            \asrt{\nt{#3-expr}{1}\att{stype} \neq $ undefined$}
            \asrt{\nt{#1-expr}{2}\att{stype} = \nt{#3-expr}{1}\att{stype}}
            \attr{\nt{#1-expr}{1}\att{stype}}{\tm{bool}}
        \end{semantic}

        The runtime phase:
        \begin{semantic}
            \If{\nt{#1-expr}{2}\att{value} #4 \nt{#3-expr}{1}\att{value}}
            \attr{\nt{#1-expr}{1}\att{value}}{\tm{true}}
            \Else
            \attr{\nt{#1-expr}{1}\att{value}}{\tm{false}}
            \Endif
        \end{semantic}
    }

    \subsubsection{Equality Expression}
    \eqexpr{equality}{==}{comparison}{=}

    \subsubsection{Inequality Expression}
    \eqexpr{equality}{!=}{comparison}{\neq}

    The pass through for equality also calls its own fallback during default type checking.

    \begin{syntax}
        \prdn{\nt{equality-expr}{1}}{\nt{comparison-expr}{1}}
    \end{syntax}

    The default type checking phase:
    \begin{semantic}
        \attr{\nt{comparison-expr}{1}\att{itype}}{\nt{equality-expr}{1}\att{itype}}
        \tcr{comparison-expr}{1}
        \If{\nt{comparison-expr}{1}\att{stype} = $ undefined$}
        \tcf{comparison-expr}{1}
        \Endif
        \attr{\nt{equality-expr}{1}\att{stype}}{\nt{comparison-expr}{1}\att{stype}}
        \asrt{\nt{equality-expr}{1}\att{stype} \neq $ undefined $}
        \asrt{\nt{equality-expr}{1}\att{itype} = $ undefined $ \vee \nt{equality-expr}{1}\att{itype} = \nt{equality-expr}{1}\att{stype}}
    \end{semantic}

    \passrun{equality}{comparison}

    \subsection{Comparison Expressions}
    \newcommand{\compexpr}[4]
    {
        \begin{syntax}
            \prdn{\nt{#1-expr}{1}}{\nt{#2-expr}{1} \tm{#3} \nt{#2-expr}{2}}
        \end{syntax}

        The default type checking phase:
        \begin{semantic}
            \asrt{\nt{#1-expr}{1}\att{itype} = \tm{bool} \vee \nt{#1-expr}{1}\att{itype} = $ undefined$}
            \attr{\nt{#2-expr}{1}\att{itype}}{$ undefined$}
            \attr{\nt{#2-expr}{2}\att{itype}}{$ undefined$}
            \tcr{#2-expr}{1}
            \tcr{#2-expr}{2}
            \If{\nt{#2-expr}{1}\att{stype} = $ undefined $ \vee \nt{#2-expr}{2}\att{stype} = $ undefined$}
            \attr{\nt{#1-expr}{1}\att{stype}}{$ undefined$}
            \Else
            \attr{\nt{#1-expr}{1}\att{stype}}{\tm{bool}}
            \Endif
        \end{semantic}

        The fallback type checking phase:
        \begin{semantic}
            \asrt{\nt{#1-expr}{1}\att{itype} = \tm{bool} \vee \nt{#1-expr}{1}\att{itype} = $ undefined$}
            \If{\nt{#2-expr}{1}\att{stype} = $ undefined $ \wedge \nt{#2-expr}{2}\att{stype} \neq $ undefined$}
            \attr{\nt{#2-expr}{1}\att{itype}}{\nt{#2-expr}{2}\att{stype}}
            \tcf{#2-expr}{1}
            \Elif{\nt{#2-expr}{1}\att{stype} \neq $ undefined $ \wedge \nt{#2-expr}{2}\att{stype} = $ undefined$}
            \attr{\nt{#2-expr}{2}\att{itype}}{\nt{#2-expr}{1}\att{stype}}
            \tcf{#2-expr}{2}
            \Endif
            \asrt{\nt{#2-expr}{1}\att{stype} \neq $ undefined$}
            \asrt{\nt{#2-expr}{2}\att{stype} \neq $ undefined$}
            \asrt{\nt{#2-expr}{1}\att{stype} = \nt{#2-expr}{2}\att{stype}}
            \attr{\nt{#1-expr}{1}\att{stype}}{\tm{bool}}
        \end{semantic}

        The runtime phase:
        \begin{semantic}
            \If{\nt{#2-expr}{1}\att{value} #4 \nt{#2-expr}{2}\att{value}}
            \attr{\nt{#1-expr}{1}\att{value}}{\tm{true}}
            \Else
            \attr{\nt{#1-expr}{1}\att{value}}{\tm{false}}
            \Endif
        \end{semantic}
    }

    \subsubsection{Less Than Expression}
    \compexpr{comparison}{add}{<}{<}

    \subsubsection{Greater Than Expression}
    \compexpr{comparison}{add}{>}{>}

    \subsubsection{Less Than or Equal Expression}
    \compexpr{comparison}{add}{<=}{\leq}

    \subsubsection{Greater Than or Equal Expression}
    \compexpr{comparison}{add}{>=}{\geq}


    \subsubsection{Comparison Pass Through}
    \exprpass{comparison}{add}

    \subsection{Binary Expressions}
    Binary Expressions are expressions in the form of two sub-expressions separated by an infix operation.
    Typically, the \textit{itype} of a binary expression is applied to its sub-expressions, and their \texttt{stype}s are expected to match both eachother, and the \textit{itype}.
    But certain binary expressions produce values of differing type than their sub-expressions.
    An example of this is the relational operators: which produce boolean values from other primitive types of sub-expressions.\\

    There are two forms of literal type inferrence: ``vertical'' type inference and ``horizontal'' type inference.
    Vertical type inferrence is inferring type based on the types of expressions higher in the syntax tree. 
    It is performed by \textit{itype}s during the \textit{regular} phase of type checking.
    Horizontal type inferrence is inferring type based on the type of other sub-expressions in the same expression.
    It is performed during the \textit{fallback} phase of type checking when one sub-expression has an \textit{stype} and the other does not.
    When this occurs, the well-defined \textit{stype} of one sub-expression is used as the \textit{itype} of the other.\\

    \newcommand{\bexpr}[5]
    {
        \begin{syntax}
            \prdn{\nt{#1-expr}{1}}{\nt{#1-expr}{2} \tm{#3} \nt{#2-expr}{1}}
        \end{syntax}

        The regular type checking phase:
        \begin{semantic}
            \attr{\nt{#1-expr}{2}\att{itype}}{\nt{#1-expr}{1}\att{itype}}
            \attr{\nt{#2-expr}{1}\att{itype}}{\nt{#1-expr}{1}\att{itype}}
            \tcr{#1-expr}{2}
            \tcr{#2-expr}{1}
            \asrt{\nt{#1-expr}{2}\att{stype} = $ undefined $ \vee \nt{#2-expr}{1}\att{stype} = $ undefined $}
            & $\vee \nt{#1-expr}{2}\att{stype} = \nt{#2-expr}{1}\att{stype} $ & \\
            \If{\nt{#1-expr}{2} = $ undefined $ \vee \nt{#2-expr}{1} = $ undefined$}
            \attr{\nt{#1-expr}{1}\att{stype}}{$ undefined$}
            \Else 
            \attr{\nt{#1-expr}{1}\att{stype}}{\nt{#1-expr}{2}\att{stype}}
            \Endif
            \asrt{\nt{#1-expr}{1}\att{itype} = $ undefined $ \vee \nt{#1-expr}{1}\att{stype} = \nt{#1-expr}{1}\att{itype}}
            #5
        \end{semantic}

        The fallback type checking phase:
        \begin{semantic}
            \attr{\nt{#1-expr}{2}\att{itype}}{\nt{#1-expr}{1}\att{itype}}
            \attr{\nt{#2-expr}{1}\att{itype}}{\nt{#1-expr}{1}\att{itype}}
            \tcr{#1-expr}{2}
            \tcr{#2-expr}{1}
            \If{\nt{#1-expr}{2}\att{stype} = $ undefined $ \wedge \nt{#2-expr}{1}\att{stype} \neq $ undefined$}
            \asrt{\nt{#1-expr}{1}\att{itype} = \nt{#2-expr}{1}\att{stype}}
            \attr{\nt{#1-expr}{2}\att{itype}}{\nt{#2-expr}{1}\att{stype}}
            \tcf{#1-expr}{2}
            \Elif{\nt{#1-expr}{2}\att{stype} \neq $ undefined $ \wedge \nt{#2-expr}{1}\att{stype} = $ undefined$}
            \asrt{\nt{#1-expr}{1}\att{itype} = \nt{#1-expr}{2}\att{stype}}
            \attr{\nt{#2-expr}{1}\att{itype}}{\nt{#1-expr}{2}\att{stype}}
            \tcf{#2-expr}{1}
            \Elif{\nt{#1-expr}{2}\att{stype} = $ undefined $ \wedge \nt{#2-expr}{1}\att{stype} = $ undefined$}
            \tcf{#1-expr}{2}
            \tcf{#2-expr}{1}
            \Endif
            \asrt{\nt{#1-expr}{2}\att{stype} = \nt{#2-expr}{1}\att{stype} \wedge \nt{#1-expr}{2}\att{stype} \neq $ undefined$}
            \asrt{\nt{#1-expr}{1}\att{itype} = $ undefined $ \vee \nt{#1-expr}{2}\att{stype} = \nt{#1-expr}{1}\att{itype}}
            \attr{\nt{#1-expr}{1}\att{stype}}{\nt{#1-expr}{2}\att{stype}}
            #5
        \end{semantic}

        The runtime phase:
        \begin{semantic}
            \run{#1-expr}{2}
            \run{#2-expr}{1}
            \attr{\nt{#1-expr}{1}}{\nt{#1-expr}{2} #4 \nt{#2-expr}{1}}
        \end{semantic}
    }

    \subsubsection{Addition Expressions}
    Addition expressions perform numerical addition and subtraction.
    They have the same precedence level, and evaluate from left to right.
    They expect their sub-expressions to be of numerical type, and produce a value of equivelant numerical type.\\

    \bexpr{add}{mult}{+}{+}{\asrt{$ is numeric? $ \nt{add-expr}{1}\att{stype}}}

    \bexpr{add}{mult}{-}{-}{\asrt{$ is numeric? $ \nt{add-expr}{1}\att{stype}}}

    \exprpass{add}{mult}

    \subsubsection{Multiplicitave Expressions}
    Multiplicitave expressions perform numerical multiplication and division.
    They have the same precedence level, and evaluate from left to right.
    They expect their sub-expressions to be of numerical type, and produce a value of equivelant numerical type.\\
    \bexpr{mult}{as}{*}{*}{\asrt{$ is numeric? $ \nt{add-expr}{1}\att{stype}}}

    \bexpr{mult}{as}{/}{/}{\asrt{$ is numeric? $ \nt{add-expr}{1}\att{stype}}}

    \exprpass{mult}{as}

    \subsection{Conversion Expressions}
    Conversion Expressions convert the value of one expression to another type.
    The \textit{itype} of the expression is checked against the \textit{type} symbol, instead of the sub-expression.
    The \textit{itype} of the sub-expression is given as undefined.

    \begin{syntax}
        \prdn{\nt{as-expr}{1}}{\nt{as-expr}{2} \tm{as} \nt{type}{1}}
    \end{syntax}

    The regular type checking phase:
    \begin{semantic}
        \attr{\nt{as-expr}{2}\att{itype}}{$ undefined$}
        \tcr{as-expr}{2}
        \If{\nt{as-expr}{2}\att{stype} = $ undefined$}
        \tcf{as-expr}{2}
        \Endif
        \asrt{\nt{as-expr}{2}\att{stype} \neq $ undefined $ \wedge \nt{as-expr}{2}\att{stype} $ converts-to? $ \nt{type}{1}}
        \attr{\nt{as-expr}{1}\att{stype}}{\nt{type}{1}}
        \asrt{\nt{as-expr}{1}\att{itype} = $ undefined $ \vee \nt{as-expr}{1}\att{stype} = \nt{as-expr}{1}\att{itype}}
    \end{semantic}

    The fallback typechecking phase:
    \begin{semantic}
        \asrt{fail}
    \end{semantic}

    The runtime phase:
    \begin{semantic}
        \attr{\nt{as-expr}{1}\att{value}}{\nt{as-expr}{2}\att{value} $ converted-to $ \nt{type}{1}}
    \end{semantic}

    \exprpass{as}{sign}

    \subsection{Sign Expressions}
    Sign expressions change the sign of numeric types.
    With a \texttt{+} it makes the value a positive value, by taking the absolute value.
    With a \texttt{-} it nakes the value, such that if it was positive it is now negative, and if it was negative it is now positive.
    Sign expressions are expected to have signed numeric types.

    \newcommand{\uexpr}[4]
    {
        \begin{syntax}
            \prdn{\nt{#1-expr}{1}}{\tm{#2} \nt{#1-expr}{2}}
        \end{syntax}

        The regular type checking phase:
        \begin{semantic}
            \attr{\nt{#1-expr}{2}\att{itype}}{\nt{#1-expr}{1}\att{itype}}
            \tcr{#1-expr}{2}
            \attr{\nt{#1-expr}{1}\att{stype}}{\nt{#1-expr}{2}\att{stype}}
            \asrt{\nt{#1-expr}{1}\att{itype} = $ undefined $ \vee \nt{#1-expr}{1}\att{stype} = \nt{#1-expr}{1}\att{itype}}
            #4
        \end{semantic}

        The fallback type checking phase:
        \begin{semantic}
            \attr{\nt{#1-expr}{2}\att{itype}}{\nt{#1-expr}{1}\att{itype}}
            \tcf{#1-expr}{2}
            \attr{\nt{#1-expr}{1}\att{stype}}{\nt{#1-expr}{2}\att{stype}}
            \asrt{\nt{#1-expr}{1}\att{stype} \neq $ undefined $}
            \asrt{\nt{#1-expr}{1}\att{itype} = $ undefined $ \vee \nt{#1-expr}{1}\att{stype} = \nt{#1-expr}{1}\att{itype}}
            #4
        \end{semantic}

        The runtime phase:
        \begin{semantic}
            \attr{\nt{#1-expr}{1}\att{value}}{#3 \nt{#1-expr}{2}\att{value}}
        \end{semantic}
    }

    \uexpr{sign}{+}{$ absolute value $}{\asrt{$ is\_signed\_numeric$ (\nt{sign-expr}{1}\att{itype})}}

    \uexpr{sign}{-}{0 -}{\asrt{$ is\_signed\_numeric$ (\nt{sign-expr}{1}\att{itype})}}

    \exprpass{sign}{parenthesis}

    \subsection{Parenthesis Expression}
    Parenthesis expressions exist to raise the precedence of their contained expression.
    The \textit{itype}, \textit{stype}, and \textit{value} of the expression and its sub-expression are simply moved upwards and downwards in the syntax tree.\\

    \begin{syntax}
        \prdn{\nt{parenthesis-expr}{1}}{\tm{(} \nt{expr}{1} \tm{)}}
    \end{syntax}

    \passtc{parenthesis-expr}{expr}{}

    \passfalltc{parenthesis-expr}{expr}{}

    \passrun{parenthesis-expr}{expr}{}

    \exprpass{parenthesis}{leaf}

    \subsection{Leaf Expressions}
    Leaf expressions are the leaves of the syntax tree.

    \subsubsection{Identifier Expressions}
    An identifier expression looks up its value as a variable in the symbol table.
    The \textit{stype} and \textit{value} both are retrieved from the symbol table.

    \begin{syntax}
        \prdn{\nt{leaf-expr}{1}}{\id{1}}
    \end{syntax}

    The regular type checking phase:
    \begin{semantic}
        \asrt{\tblhas{1}}
        \asrt{\tblget{1}\att{kind} = $ variable$}
        \attr{\nt{leaf-expr}{1}\att{stype}}{\tblget{1}\att{type}}
        \asrt{\nt{leaf-expr}{1}\att{stype} = \nt{postfix-expr}{1}\att{itype}}
    \end{semantic}

    The fallback type checking phase:
    \begin{semantic}
        \asrt{$failure$}
    \end{semantic}

    The runtime phase:
    \begin{semantic}
        \attr{\nt{leaf-expr}{1}\att{value}}{\tblget{1}\att{value}}
    \end{semantic}

    \subsubsection{Integer Literal Expressions}
    An integer literal has a numeric value which is determined from the numerals which compose it.
    This value is converted to the correct type using vertical type inferrence.
    During \textit{regular} type checking, it attempts to take \textit{itype} as the \textit{stype}, but if the \textit{itype} is undefined, it remains undefined.
    During \textit{fallback} type checking, it takes the \textit{itype} if it is defined, otherwise it takes the default type of \texttt{s}.

    \begin{syntax}
        \prdn{\nt{leaf-expr}{1}}{\st{literal-int}}
    \end{syntax}

    The regular type checking phase:
    \begin{semantic}
        \asrt{\nt{leaf-expr}{1}\att{itype} =$ undefined $ \vee $ is\_integer\_type$($ $\nt{leaf-expr}{1}\att{itype})}
        \attr{\nt{leaf-expr}{1}\att{stype}}{\nt{leaf-expr}{1}\att{itype}}
    \end{semantic}

    The fallback type checking phase:
    \begin{semantic}
        \asrt{\nt{leaf-expr}{1}\att{itype} =$ undefined $ \vee $ is\_integer\_type$($ $\nt{leaf-expr}{1}\att{itype})}
        \attr{\nt{leaf-expr}{1}\att{stype}}{$ if $ \nt{leaf-expr}{1}\att{itype} \neq $ undefined then $ \nt{leaf-expr}{1}\att{itype} $ else $ \tm{s}}
    \end{semantic}

    The runtime phase:
    \begin{semantic}
        \asrt{$in\_integer\_range$($ $\st{literal-int}\att{value}, \nt{leaf-expr}{1}\att{stype})}
        \attr{\nt{leaf-expr}{1}\att{value}}{\st{literal-int}\att{value}}
    \end{semantic}
\end{document}
